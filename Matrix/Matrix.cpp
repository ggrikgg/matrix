// Matrix.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "iostream"
#include <string>
namespace mymatr
{
	static int Error(const std::string &s)
	{
		std::cerr << "Error: " << s << '\n';
		return 1;
	}


	template <typename T>
	class Matrix;
	template <typename T>
	std::ostream& operator<<(std::ostream&, const Matrix<T>&);
	template <typename T>
	std::istream& operator>>(std::istream&, Matrix<T>&);


	template <typename T>
	class Matrix
	{
	public:
		Matrix()
		{
			matrix_ = nullptr;
		}
		Matrix(const int size);

		Matrix(T** arr, const int height, const int width )
		{
			(*this).MatrixCopy(arr,  height, width);
		}

		Matrix(const T** arr, const int height, const int width)
		{
			(*this).MatrixCopy(arr, height, width);
		}
		Matrix(const Matrix<T>& obj);

		~Matrix();

		double* &operator[] (const int);
		double* &operator[] (const int) const;
		Matrix<T>& operator=(const Matrix<T>&);
		Matrix<T>& operator+=(const Matrix<T>&);
		Matrix<T>& operator-=(const Matrix<T>&);
		Matrix<T>& operator*=(const T&);
		Matrix<T>& operator*=(const Matrix<T>&);
		template <typename T>
		friend Matrix operator+(const Matrix&, const Matrix&);
		friend Matrix<T> operator-(const Matrix<T>&, const Matrix<T>&);
		friend Matrix<T> operator*(const Matrix<T>&, const Matrix<T>&);
		friend bool operator==(const Matrix<T>&, const Matrix<T>&);
		friend std::istream& operator>> <>(std::istream&, Matrix<T>&);
		friend std::ostream& operator<< <>(std::ostream&, const Matrix<T>&);

	private:
		double **matrix_=nullptr;
		int height_;
		int width_;

		void MatrixDel();
		void MatrixCopy(T** arr,  const int height, const int width);
		void MatrixCopy(const T** arr, const int height, const int width);
	};

	template <typename T>
	Matrix<T>::~Matrix() 
	{
		if (matrix_ != nullptr)
		{
			for (int i = 0; i < height_; ++i)
				delete[] matrix_[i];
			delete[] matrix_;
			matrix_ = nullptr;
		}
	}

	template <typename T>
	Matrix<T>::Matrix(const int size)
	{
		height_ = size;
		width_ = size;
		try
		{
			matrix_ = new double*[height_]; // ��� ������ � �������
		}
		catch (std::bad_alloc& ba)
		{
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return;
		}
		int count;
		try
		{
			for (count = 0; count < height_; count++)
				matrix_[count] = new double[width_];
		}
		catch (std::bad_alloc& ba)
		{
			for (int i = 0; i < count; i++)
				delete[](matrix_[i]);
			delete[] matrix_;
			matrix_ = nullptr;
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return;
		}

	}

	template <typename T>
	Matrix<T>::Matrix(const Matrix& obj)
	{
		if (matrix_ != nullptr)
			for (int count = 0; count < height_; count++)
				delete[](matrix_[count]);
		height_ = obj.height_;
		width_ = obj.width_;
		try
		{
			matrix_ = new T*[height_]; // ��� ������ � �������
		}
		catch (std::bad_alloc& ba)
		{
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return;
		}
		int count;
		try
		{
			for (count = 0; count < height_; count++)
				matrix_[count] = new T[width_];
		}
		catch (std::bad_alloc& ba)
		{
			for (int i = 0; i < count; i++)
				delete[](matrix_[i]);
			delete[] matrix_;
			matrix_ = nullptr;
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return;
		}
			for (int i = 0; i < height_; i++)
				for (int j = 0; j < width_; j++)
					matrix_[i][j] = obj.matrix_[i][j];

	}

	template <typename T>
	Matrix<T> operator+(const Matrix<T>& obj1, const Matrix<T>& obj2)
	{
		Matrix<T> Matr(obj1);
		return Matr += obj2;
	}
	template <typename T>
	Matrix<T> operator-(const Matrix<T>& obj1, const Matrix<T>& obj2)
	{
		Matrix Matr(obj1);
		return Matr -= obj2;
	}
	template <typename T>
	Matrix<T>& Matrix<T>::operator+=(const Matrix<T>& obj)
	 {
		if (height_ != obj.height_ || width_ != obj.width_ || matrix_==nullptr || obj.matrix_==nullptr)
		{
			Error("Assignment error!");
			return *this;
		}
		for (int i = 0; i < height_; ++i)
			for (int j = 0; j < width_; ++j)
				matrix_[i][j] += obj.matrix_[i][j];
		
		return *this;
		}

	template <typename T>
	Matrix<T>& Matrix<T>::operator-=(const Matrix<T>& obj)
	{
		if (height_ != obj.height_ || width_ != obj.width_ || matrix_ == nullptr || obj.matrix_ == nullptr)
		{
			Error("Assignment error!");
			return *this;
		}
		for (int i = 0; i < height_; ++i)
			for (int j = 0; j < width_; ++j)
				matrix_[i][j] -= obj.matrix_[i][j];

		return *this;
	}
	template <typename T>
	Matrix<T>& Matrix<T>::operator*=(const T& number)
	{
		if (matrix_ == nullptr)
		{
			Error("Assignment error!");
			return *this;
		}
		for (int i = 0; i < height_; ++i)
			for (int j = 0; j < width_; ++j)
				matrix_[i][j] *= number;

		return *this;
	}
	template <typename T>
	Matrix<T> operator*(const Matrix<T>& obj1, const Matrix<T>& obj2)
		{
			Matrix matr(obj1);
			return matr *= obj2;
		}
	template <typename T>
	Matrix<T>& Matrix<T>::operator*=(const Matrix<T>& obj)
	{
		if (width_!= obj.height_||  matrix_ == nullptr || obj.matrix_ == nullptr)
		{
			Error("Assignment error!");
			return *this;
		}
		int tempwidth = obj.width_; 
		int tempheight = height_;
		double** tempmatr;

		try
		{
			tempmatr = new T*[tempheight]; // ��� ������ � �������
		}
		catch (std::bad_alloc& ba)
		{
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return *this;
		}
		int count;
		try
		{
			for (count = 0; count <tempheight; count++)
				tempmatr[count] = new T[tempwidth];
		}
		catch (std::bad_alloc& ba)
		{
			for (int i = 0; i < count; i++)
				delete[](matrix_[i]);
			delete[] matrix_;
			matrix_ = nullptr;
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return *this;
		}



		for (int i = 0; i < tempheight; ++i)
			for (int j = 0; j < tempwidth; ++j)
			{
				tempmatr[i][j] = 0;
				for (int k = 0; k < width_; ++k)
					tempmatr[i][j] = tempmatr[i][j] + matrix_[i][k] *obj.matrix_[k][j];		
			}
		height_ = tempheight;
		width_ = tempwidth;
		(*this).MatrixDel();
		matrix_ = tempmatr;

		return *this;
	}
	template <typename T>
	bool operator==(const Matrix<T>& obj1, const Matrix<T>& obj2)
	{
		if (obj1.height_ == obj2.height_ && obj1.width_ == obj2.width_)
		{
			for (int i = 0; i < obj1.height_; ++i)
				for (int j = 0; j < obj1.width_; ++j)
					if (obj1.matrix_[i][j] != obj2.matrix_[i][j])
						return false;
		}
		else 
			return false;

		return true;
	}
	template <typename T>
	void Matrix<T>::MatrixCopy(const T** arr, const int height, const int width)
	{

		if (matrix_ != nullptr)
			for (int count = 0; count < height_; count++)
				delete[](matrix_[count]);
		height_ = height;
		width_ = width;

		try
		{
			matrix_ = new T*[height_]; // ��� ������ � �������
		}
		catch (std::bad_alloc& ba)
		{
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return;
		}
		int count;
		try
		{
			for (count = 0; count < height_; count++)
				matrix_[count] = new T[width_];
		}
		catch (std::bad_alloc& ba)
		{
			for (int i = 0; i < count; i++)
				delete[](matrix_[i]);
			delete[] matrix_;
			matrix_ = nullptr;
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return;
		}

		for (int i = 0; i < height_; i++)
			for (int j = 0; j < width_; j++)
				matrix_[i][j] = arr[i][j];

	}
	template <typename T>
	void Matrix<T>::MatrixCopy(T** arr, const int height, const int width)
	{

		if (matrix_ != nullptr)
			for (int count = 0; count < height_; count++)
				delete[](matrix_[count]);
		height_ = height;
		width_ = width;

		try
		{
			matrix_ = new T*[height_]; // ��� ������ � �������
		}
		catch (std::bad_alloc& ba)
		{
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return;
		}
		int count;
		try
		{
			for (count = 0; count < height_; count++)
				matrix_[count] = new T[width_];
		}
		catch (std::bad_alloc& ba)
		{
			for (int i = 0; i < count; i++)
				delete[](matrix_[i]);
			delete[] matrix_;
			matrix_ = nullptr;
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return;
		}

		for (int i = 0; i < height_; i++)
			for (int j = 0; j < width_; j++)
				matrix_[i][j] = arr[i][j];

	}
	template <typename T>
	void Matrix<T>::MatrixDel()
	{ 
		if (matrix_ != nullptr)
			try
		{
			for (int count = 0; count < height_; count++)
				delete[](matrix_[count]);
		}
		catch (std::exception& e)
		{
			std::cerr << "exception caught: " << e.what() << '\n';
		}
	}
	template <typename T>
	Matrix<T>& Matrix<T>::operator=(const Matrix<T>& obj)
		{
		if (this != &obj)
		{
			(*this).MatrixCopy(obj.matrix_, obj.width_, obj.height_);
		}
		return *this;
		}
	template <typename T>
	std::ostream& operator<< <>(std::ostream& os, const Matrix<T>& obj)
		 {
			for (int i = 0; i < obj.height_; i++)
			{
				for (int j = 0; j < obj.width_; j++)
					os << obj.matrix_[i][j]<<' ';
				std::cout << std::endl;
			}
			return os;
		}
	template <typename T>
	std::istream& operator>> <>(std::istream& is, Matrix<T>& obj)
		{
		if (obj.matrix_!=nullptr)
			for (int count = 0; count < obj.height_; count++)
				delete[](obj.matrix_[count]);

		is >> obj.width_>>obj.height_;
		try
		{
			obj.matrix_ = new T*[obj.height_]; // ��� ������ � �������
		}
		catch (std::bad_alloc& ba)
		{
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return is;
		}
		int count;
		try
		{
			for (count = 0; count < obj.height_; count++)
				obj.matrix_[count] = new T[obj.width_];
		}
		catch (std::bad_alloc& ba)
		{
			for (int i = 0; i < count; i++)
				delete[](obj.matrix_[i]);
			delete[] obj.matrix_;
			obj.matrix_ = nullptr;
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			return is;
		}

		for (int i = 0; i < obj.height_; ++i)
			for (int j = 0; j < obj.width_; ++j)
				is >> obj.matrix_[i][j];	
			return is;
		}

	template <typename T>
	double* &Matrix<T>::operator[] (const int height)
	{
		if (height< 0 || height >= height_)
		{
			Error("Index out of range");
		}
		return matrix_[height]; 
	}

	template <typename T>
	double* &Matrix<T>::operator[] (const int height) const
	{
		if (height< 0 || height >= height_)
		{
			Error("Index out of range");
		}
		return matrix_[height];
	}

}


int main()
{
	double **d = new double*[2];
	for (int i = 0; i < 2; ++i)
	{
		d[i] = new double[3];
		for (int j = 0; j < 3; ++j)
			d[i][j] = 2;
	}
	double **c = new double*[3];
	for (int i = 0; i < 3; ++i)
	{
		c[i] = new double[3];
		for (int j = 0; j < 3; ++j)
		{
			c[i][j] = 1;
		}
	}
	mymatr::Matrix<double> ab(d,2,2);
	mymatr::Matrix<double> addd(c, 3, 3);
	mymatr::Matrix<double> a;
	a = ab;
	a =a+ab;
	std::cout << a << addd;
	addd *= a;
//	std::cin >> ab;
	std::cout <<addd[1][1];
	system("pause");
    return 0;
}

